package task2;

public class ListNode {
    private int value;
    private ListNode next;

    public ListNode(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public ListNode getNext() {
        return next;
    }

    public void setNext(ListNode next) {
        this.next = next;
    }

    public void setValue(int value) {
        this.value = value;
    }
}

class List {
    private ListNode head;
    private int size;

    public List() {
        this.head = null;
        this.size = 0;
    }

    public void add(int element) {
        add(element, size);
    }

    public void add(int element, int index) {
        if (index > size || index < 0) {
            throw new IndexOutOfBoundsException();
        }

        ListNode newNode = new ListNode(element);
        if (index == 0) {
            newNode.setNext(head);
            head = newNode;
        } else {
            ListNode prev = head;
            for (int i = 0; i < index - 1; i++) {
                prev = prev.getNext();
            }
            newNode.setNext(prev.getNext());
            prev.setNext(newNode);
        }
        size++;
    }

    public int delete(int index) {
        if (index >= size || index < 0) {
            throw new IndexOutOfBoundsException();
        }

        int deletedValue;
        if (index == 0) {
            deletedValue = head.getValue();
            head = head.getNext();
        } else {
            ListNode prev = head;
            for (int i = 0; i < index - 1; i++) {
                prev = prev.getNext();
            }
            deletedValue = prev.getNext().getValue();
            prev.setNext(prev.getNext().getNext());
        }
        size--;
        return deletedValue;
    }

    public void replace(int element, int index) {
        if (index >= size || index < 0) {
            throw new IndexOutOfBoundsException();
        }

        ListNode current = head;
        for (int i = 0; i < index; i++) {
            current = current.getNext();
        }
        current.setValue(element);
    }

    public int get(int index) {
        if (index >= size || index < 0) {
            throw new IndexOutOfBoundsException();
        }

        ListNode current = head;
        for (int i = 0; i < index; i++) {
            current = current.getNext();
        }
        return current.getValue();
    }

    public int size() {
        return size;
    }
}

