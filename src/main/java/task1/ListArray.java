package task1;

import java.util.Arrays;

public class ListArray {
    private int[] array;
    private int size;

    public ListArray() {
        this.array = new int[10];
        this.size = 0;
    }

    public void add(int element) {
        add(element, size);
    }

    public void add(int element, int index) {
        if (index > size || index < 0) {
            throw new IndexOutOfBoundsException();
        }

        if (size == array.length) {
            array = Arrays.copyOf(array, array.length * 2);
        }

        for (int i = size - 1; i >= index; i--) {
            array[i + 1] = array[i];
        }
        array[index] = element;
        size++;
    }

    public int delete(int index) {
        if (index >= size || index < 0) {
            throw new IndexOutOfBoundsException();
        }

        int deletedElement = array[index];
        for (int i = index; i < size - 1; i++) {
            array[i] = array[i + 1];
        }
        size--;
        return deletedElement;
    }

    public void replace(int element, int index) {
        if (index >= size || index < 0) {
            throw new IndexOutOfBoundsException();
        }
        array[index] = element;
    }

    public int get(int index) {
        if (index >= size || index < 0) {
            throw new IndexOutOfBoundsException();
        }
        return array[index];
    }

    public int size() {
        return size;
    }
}

